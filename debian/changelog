trinfo (0.9.4) stable; urgency=medium

  * Rename "free_version" variable. Prevents debian/rules from messing
    it up.

 -- Filip Danilovic <filip@openmailbox.org>  Mon, 01 Feb 2016 17:23:05 +0100

trios-info (0.9.3) stable; urgency=medium

  * Switch to fixed width font in Report window
  * If pacman is found, include it's main & mirrors configs in full
    report.

 -- Filip Danilovic <filip@openmailbox.org>  Mon, 01 Feb 2016 17:00:00 +0100

trios-info (0.9.2) stable; urgency=medium

  * Properly calculate used RAM on systems with procps-ng 3.3.10 and newer.
  * Remove units (MB) from disk usage display. "df" allready adds them.

 -- Filip Danilovic <filip@openmailbox.org>  Sun, 31 Jan 2016 00:51:53 +0100

trios-info (0.9.1) stable; urgency=medium

  * [Detailed report] Include dmesg, pkg manager config and more inxi stuff.
  * [Report Win] Use distro name in title & set subtitle according to mode.
  * debian/control:
    - Bump dh build-dep to v9

 -- Filip Danilovic <filip@openmailbox.org>  Mon, 18 Jan 2016 10:33:05 +0100

trios-info (0.9.0) stable; urgency=medium

  [Filip Danilovic]
  * Add more commands to Detailed report & use distro name in tittle.
  * Add "overlay" file system. Prevents crash on live systems using it.
  * [GTK] Add option for exporting report to file, or pastebin.
  * [GTK] More renaming + "help" and "about" info update...
  * [GTK] Add ability to generate full system report + cleanup + some renaming...

  [Milos Pavlovic]
  * Wrap long lines to keep main window size
  * Try to display correct Window Manager info in terminal
  * Try to display correct Window Manager name in gtk version
  * Fix won't start in Fluxbox Define variable "de" as "None" when no DE can be found

  [Eldin Gagulic]
  * replace free with /usr/bin/free because of funny alias

 -- Filip Danilovic <filip@openmailbox.org>  Mon, 18 Jan 2016 03:12:03 +0100

trios-info (0.8.4) stable; urgency=medium

  [Milos Pavlovic]
  * GTK:
    - Explicitly require gtk3,
    - Detect and display default terminal only on Debian based distros.

 -- Filip Danilovic <filip@openmailbox.org>  Mon, 21 Dec 2015 02:15:31 +0100

trios-info (0.8.3) stable; urgency=medium

  * CLI:
    - Properly detect running terminal,
    - Code cleanup.
  * GTK:
    - Avoid crash on terminal detection on non Debian distributons,
    - Rename "Terminal" label to "Default Terminal".

 -- Filip Danilovic <filip@openmailbox.org>  Fri, 18 Dec 2015 20:18:31 +0100

trios-info (0.8.2) stable; urgency=medium

  * Workaround for package number detection on pacman based distributions.

 -- Filip Danilovic <filip@openmailbox.org>  Fri, 18 Dec 2015 17:22:24 +0100

trios-info (0.8.1) stable; urgency=medium

  * Fix crash on live systems, caused by "df" missing "aufs" type entry.

 -- Filip Danilovic <filip@openmailbox.org>  Wed, 21 Oct 2015 00:55:31 +0200

trios-info (0.8) stable; urgency=medium

  * Add option to call inxi from CLI version.
  * Check if framebuffer exists before attempting to take screenshot
    with fbgrab.

 -- Filip Danilovic <filip@openmailbox.org>  Tue, 20 Oct 2015 14:18:29 +0200

trios-info (0.7) stable; urgency=medium

  * Don't use loop to wait for screenshot file to be written.
  * Remove "Taking screenshot in n seconds" message in time.
  * If uploading fails, make sure to print "request" stderr as well.

 -- Filip Danilovic <filip@openmailbox.org>  Tue, 20 Oct 2015 01:52:32 +0200

trios-info (0.6) stable; urgency=medium

  * Add option to upload screenshot to imgur.
  * Screenshot related output:
    - Print screenshot filename and path,
    - Cleanup.

 -- Filip Danilovic <filip@openmailbox.org>  Sun, 18 Oct 2015 23:10:43 +0200

trios-info (0.5) stable; urgency=medium

  * Added ability to take screenshot from tty.
  * debian/control:
    - Recommend fbcat

 -- Filip Danilovic <filip@openmailbox.org>  Thu, 15 Oct 2015 02:12:47 +0200

trios-info (0.4) stable; urgency=medium

  * Add ability to take screenshot ( CLI version only, using "scrot" )
  * Revert "cinnamon-session" typo fix, wasn't actuall typo, but "ps" limit.
  * debian/control:
    - Add scrot to recommends

 -- Filip Danilovic <filip@openmailbox.org>  Wed, 14 Oct 2015 00:48:55 +0200

trios-info (0.3.1) stable; urgency=medium

  * Add supoprt for arguments.
  * Add version information.
  * debian/rules:
    - Set version acquired from changelog in py files.

 -- Filip Danilovic <filip@openmailbox.org>  Mon, 12 Oct 2015 23:54:02 +0200

trios-info (0.3) stable; urgency=medium

  [Filip Danilovic]
  * All:
    - Fix typo in DE's dict @cinnamon-session.

  * trinfo:
    - Use "lsb_release" for fetching distribution name.

  * trinfo-gtk:
    - Reworked images/icons:
      Logo: Try loading the specified image. If that fails, try loading 
            "applications-system" icon from system theme. If both fail, 
            complain and exit.
      Window icons: Try loading a couple of icons from system theme. If 
                    it fails, use logo instead.

  * Renamed some functions/widgets to be more descriptive.

  * Added underline shortcuts to buttons in Detailed report.

  * "Copy to clipboard" & "Detailed report" buttons have focus by default.

 -- Filip Danilovic <filip@openmailbox.org>  Sun, 11 Oct 2015 22:44:08 +0200

trios-info (0.2) stable; urgency=medium

  [Filip Danilovic]
  * CLI: Added GPU info.
  * GTK: Added detailed system info, uses inxi.
  * GTK: Added ability to copy inix output to clipboard.

  [Milos Pavlovic]
  * Added GTK GUI.
  * GTK: Added about dialog.
  * GTK: Added level bars for RAM and disk usage.
  * Fixed color change condition.

  * Various other small fixes and improvements.

 -- Filip Danilovic <filip@openmailbox.org>  Wed, 30 Sep 2015 00:09:10 +0200

trios-info (0.1) stable; urgency=low

  * Initial release. (Closes: #999999)

 -- Filip Danilovic <filip@openmailbox.org>  Sat, 12 Sep 2015 14:12:43 +0200
